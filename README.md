# Image gallery


First of all we need install dependencies with command
```
npm i
```

Next, for start gallery run
```
npm run start:prod:gzip
```
For develop graph (in a separate terminal) run
```
npm start
```
For compile production version run
```
npm run build:prod:gzip
```

### Technology Stack

* ECMAScript6
* Webpack
* Babel
* SASS
* TypeScript

### Requirements

* NodeJS 6.+
* npm 3.+

### NPM scripts

Serve application to browser

```
npm start
```

Compiling options

```
npm start                  # start server
npm run start:prod         # start server with minification
npm run start:prod:gzip    # start server with minification and gzip
npm run build              # build 
npm run build:prod         # build with minification
npm run build:prod:gzip    # build with minification and gzip
```

For use gallery you need:
------
* Next structure
```
<div id="you-own-gallery-id" class="gallery-container">
    <div class="gallery-wrapper">
        <div class="gallery-slide"></div>
    </div>
</div>
```
* And in your script you need initialize gallery
```
new Gallery('you-own-gallery-id', {});
```

* _important_ gallery support next options 
```
options = {
    autoPlay: false,
    speed: 3000
};
```