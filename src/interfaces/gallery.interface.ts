export interface IGalleryOptions {
  autoPlay?: boolean,
  speed?: number
}