export class Swipe {
  private element: any;
  private xDown: any;
  private yDown: any;
  private xDiff: any;
  private yDiff: any;

  constructor(element: any) {
    this.xDown = null;
    this.yDown = null;
    this.element = typeof(element) === 'string' ? document.querySelector(element) : element;

    this.element.addEventListener('touchstart', function(e: any) {
      this.xDown = e.touches[0].clientX;
      this.yDown = e.touches[0].clientY;
    }.bind(this), false);

  }

  onLeft(cb?: any) {
    if (cb) this.onLeft = cb;
    return this;
  }

  onRight(cb?: any) {
    if (cb) this.onRight = cb;
    return this;
  }

  onUp(cb?: any) {
    if (cb) this.onUp = cb;
    return this;
  }

  onDown(cb?: any) {
    if (cb) this.onDown = cb;
    return this;
  }

  handleTouchMove(e: any) {
    if ( ! this.xDown || ! this.yDown ) {
      return;
    }

    let xUp = e.touches[0].clientX;
    let yUp = e.touches[0].clientY;

    this.xDiff = this.xDown - xUp;
    this.yDiff = this.yDown - yUp;

    if ( Math.abs( this.xDiff ) > Math.abs( this.yDiff ) ) {
      if ( this.xDiff > 0 ) {
        this.onLeft();
      } else {
        this.onRight();
      }
    } else {
      if ( this.yDiff > 0 ) {
        this.onUp();
      } else {
        this.onDown();
      }
    }

    // Reset values.
    this.xDown = null;
    this.yDown = null;
  }

  run() {
    this.element.addEventListener('touchmove', function(e: any) {
      this.handleTouchMove(e);
    }.bind(this), false);
  }
}