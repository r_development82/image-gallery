'use strict';
const HtmlWebpack = require('html-webpack-plugin');
const CompressionPlugin = require("compression-webpack-plugin");
const path = require('path');
const webpack = require('webpack');
const rootDir = path.resolve(__dirname, './');
const ChunkWebpack = webpack.optimize.CommonsChunkPlugin;
const port = 3000;
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const extractSass = new ExtractTextPlugin({
    filename: "[name].[contenthash].css"
});

const paths = {
    contentBase: path.resolve(rootDir, 'dist'),
    template: path.resolve(rootDir, 'index.html'),
    sources: path.resolve(rootDir, 'src'),
    bundle: path.resolve(rootDir, 'src', 'index.ts'),
    styles: path.resolve(rootDir, 'src', 'styles.scss')
};

const config = {
    devServer: {
        contentBase: paths.contentBase,
        port: port
    },
    devtool: 'eval',
    entry: {
        bundle: paths.bundle,
        styles: paths.styles
    },
    module: {
        rules: [
            {
                test: /\.(png|jpe?g|gif)$/,
                loader: 'file-loader?name=public/images/[name].[ext]'
            },
            {
                test: /\.(woff|ttf|eot|woff2|svg)$/,
                loader: 'file-loader?name=public/fonts/[name].[ext]'
            },
            {
                test: /\.scss$/,
                use: extractSass.extract({
                    use: [{
                        loader: "css-loader"
                    }, {
                        loader: "sass-loader"
                    }],
                    fallback: "style-loader"
                })
            },
            {
                test: /\.ts$/,
                loader: 'ts-loader',
                options: {
                    configFile: 'tslint.json'
                }
            },
            {
                test: /\.(css|html)$/,
                loader: 'raw-loader'
            }
        ]
    },
    output: {
        filename: '[name].bundle.js',
        path: paths.contentBase
    },
    plugins: [
        new ChunkWebpack({
            filename: 'vendor.bundle.js',
            minChunks: Infinity,
            name: 'vendor'
        }),
        new HtmlWebpack({
            filename: 'index.html',
            inject: 'body',
            template: paths.template
        }),
        extractSass
    ],
    resolve: {
        extensions: [ '*', '.js', '.ts' ]
    }
};

module.exports = (env = {}) => {
    function addMinifyPlugin() {
        config.plugins.push(
            new webpack.LoaderOptionsPlugin({
                minimize: true,
                debug: false
            }),
            new webpack.optimize.UglifyJsPlugin({
                compress: {
                    warnings: false
                },
                output: {
                    comments: false
                },
                sourceMap: false
            })
        );
    }

    function addGzipPlagin() {
        config.plugins.push(
            new CompressionPlugin({
                asset: "[path].gz[query]",
                algorithm: "gzip",
                test: /\.js$|\.html$/,
                threshold: 10240,
                minRatio: 0.8
            })
        );

        config.devServer.compress = true;
    }

    if (env.env === 'prod') {
        addMinifyPlugin();
    } else if (env.env === 'prod:gzip') {
        addMinifyPlugin();
        addGzipPlagin();
    }

    return config;
};